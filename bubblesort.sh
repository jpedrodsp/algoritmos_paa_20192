#!/bin/bash

currentDate=$1

g++ -o bubblesort.exe src/bubblesort/bubblesort.cpp -static -O3
for INDEX in 100 500 1000 5000 30000 80000 100000 150000 200000
do
  echo "Bubblesort with $INDEX: increasing"
  ./bubblesort.exe "increasing_$INDEX.txt" $INDEX "increasing" >> "logs/$currentDate/bubblesort_increasing_$INDEX.txt"
  echo "Bubblesort with $INDEX: decreasing"
  ./bubblesort.exe "decreasing_$INDEX.txt" $INDEX "decreasing" >> "logs/$currentDate/bubblesort_decreasing_$INDEX.txt"
  echo "Bubblesort with $INDEX: random"
  ./bubblesort.exe "random_$INDEX.txt" $INDEX "random" >> "logs/$currentDate/bubblesort_random_$INDEX.txt"
done