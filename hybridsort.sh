#!/bin/bash

currentDate=$1

g++ -o hybridsort.exe src/hybridsort/hybridsort.cpp -static -O3
for INDEX in 100 500 1000 5000 30000 80000 100000 150000 200000
do
  echo "Hybridsort with $INDEX: increasing"
  ./hybridsort.exe "increasing_$INDEX.txt" $INDEX "increasing" >> "logs/$currentDate/hybridsort_increasing_$INDEX.txt"
  echo "Hybridsort with $INDEX: decreasing"
  ./hybridsort.exe "decreasing_$INDEX.txt" $INDEX "decreasing" >> "logs/$currentDate/hybridsort_decreasing_$INDEX.txt"
  echo "Hybridsort with $INDEX: random"
  ./hybridsort.exe "random_$INDEX.txt" $INDEX "random" >> "logs/$currentDate/hybridsort_random_$INDEX.txt"
done