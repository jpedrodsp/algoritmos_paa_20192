//
// Created by erick on 04/10/2019.
//

#include <iostream>
#include <string>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <limits>

int main(int argc, char *argv[])
{
    if (argc)
    {
        printf("Received %d args, value: %s\n", argc, argv[1]);
        int n = atoi(argv[1]);
        std::cout << "Creating a file with " << n << " numbers..." << std::endl;

        std::cout << "Defining a random seed..." << std::endl;
        time_t t;
        srand((unsigned int) t);


        int i;
        int actual_value = 0;
        int data_vector[n];

        // Increasing Numbers
        std::cout << "Creating and writing increasing numbers..." << std::endl;
        char filename[256] = "";
        strcat(&filename[0], "increasing_");
        strcat(&filename[0], argv[1]);
        strcat(&filename[0], ".txt");
        FILE *increasing_file = fopen(filename, "w+");
        for (i = 0; i < n; i++)
        {
            actual_value += rand() % 5 + 1;
            data_vector[i] = actual_value;
            if (i != n-1)
                fprintf(increasing_file, "%d\n", actual_value);
            else
                fprintf(increasing_file, "%d", actual_value);
        }
        fclose(increasing_file);
        std::cout << "End of increasing numbers." << std::endl;

        // Decreasing Numbers
        std::cout << "Writing decreasing numbers..." << std::endl;
        strcpy(filename, "");
        strcat(&filename[0], "decreasing_");
        strcat(&filename[0], argv[1]);
        strcat(&filename[0], ".txt");
        FILE *decreasing_file = fopen(filename, "w+");
        for (i = n-1; i >= 0; i--)
        {
            actual_value = data_vector[i];
            if (i != 0)
                fprintf(decreasing_file, "%d\n", actual_value);
            else
                fprintf(decreasing_file, "%d", actual_value);
        }
        fclose(decreasing_file);
        std::cout << "End of decreasing numbers." << std::endl;

        // Random Numbers
        std::cout << "Creating and writing random numbers..." << std::endl;
        strcpy(filename, "");
        strcat(&filename[0], "random_");
        strcat(&filename[0], argv[1]);
        strcat(&filename[0], ".txt");
        FILE *random_file = fopen(filename, "w+");
        for (i = 0; i < n; i++)
        {
            actual_value = (int) rand() % INT32_MAX;
            if (actual_value < 0)
                actual_value *= -1;
            if (i != n-1)
                fprintf(random_file, "%d\n", actual_value);
            else
                fprintf(random_file, "%d", actual_value);
        }
        fclose(random_file);
        std::cout << "End of random numbers." << std::endl;
    }
    return 0;
}