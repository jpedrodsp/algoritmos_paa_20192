//
// Created by jpedro on 04/10/2019.
//

#include <iostream>
#include <string>
#include <ctime>
#include <chrono>
#include <cstring>

#define ARBITRARY_LARGE_SIZE 500000
#define RUN_TIMES 10
#define PROGRAM_NAME "HEAPSORT"

void print_vector(int *vector, int size) {
    int i;
    for (i = 0; i < size; i++) {
        printf("%d ", vector[i]);
    }
    printf("\n");
}

int read_file(const char *program_name, int expected_numbers, int *vector) {
    FILE *f = fopen(program_name, "r+");
    if (f) {
        int i = 0;
        while (i < expected_numbers && !feof(f)) {
            fscanf(f, "%d\n", &vector[i]);
            if (expected_numbers <= ARBITRARY_LARGE_SIZE)
                printf("%d ", vector[i]);
            i++;
        }
        printf("\n");
        return 1;
    } else {
        return 0;
    }
}

int pai(int i) {
    return (i / 2);
}

int direita(int i) {
    return (2 * i) + 1;
}

int esquerda(int i) {
    return 2 * i;
}

void heapMaximo(int *A, int i, int *t, long *comparisons) {
    int maior, e, d;

    e = esquerda(i);
    d = direita(i);
    *comparisons = *comparisons + 1;
    if (e <= *t && A[e] > A[i])
        maior = e;
    else
        maior = i;
    *comparisons = *comparisons + 1;
    if (d <= *t && A[d] > A[maior])
        maior = d;
    *comparisons = *comparisons + 1;
    if (maior != i) {
        int aux = A[i];
        A[i] = A[maior];
        A[maior] = aux;
        heapMaximo(A, maior, t, comparisons);
    }

}

void constroiHeapMaximo(int *A, int n, int *t, long *comparisons) {
    int i;
    *t = n - 1;
    *comparisons = *comparisons + 1;
    for (i = n / 2; i >= 0; i--) {
        *comparisons = *comparisons + 1;
        heapMaximo(A, i, t, comparisons);
    }
}

void heapSort(int *A, int n, int *t, long *comparisons) {
    constroiHeapMaximo(A, n, t, comparisons);
    int i;
    *comparisons = *comparisons + 1;
    for (i = n - 1; i > 0; i--) {
        *comparisons = *comparisons + 1;
        int aux = A[0];
        A[0] = A[i];
        A[i] = aux;
        *t = *t - 1;
        heapMaximo(A, 0, t, comparisons);
    }
}

void result_write(const char *test_name, int size, double result, const char *test_type, double comparisons_num) {
    char fn[256] = "";
    strcat(fn, test_name);
    strcat(fn, "_results");
    strcat(fn, ".txt");
    FILE *f;
    f = fopen(fn, "a+");
    if (f) {
        fprintf(f, "%s [%s %d]: %lf; %lf comparisons\n", test_name, test_type, size, result, comparisons_num);
        fclose(f);
    }
}

int main(int argc, char *argv[]) {
    if (argc >= 3) {
        std::cout << PROGRAM_NAME << std::endl;
        printf("Received %d args, file: %s, size: %s, type: %s\n", argc, argv[1], argv[2], argv[3]);

        int vector_size = atoi(argv[2]);
        int vector[vector_size];
        if (!read_file(argv[1], vector_size, vector)) {
            return 1;
        }
        printf("Vector filled sucessfully.\n");

        long dif = 0;
        double total_dif = 0;
        long comparisons = 0;
        double total_comparisons = 0;
        int hs_lastelement_index = vector_size - 1;
        for (int i = 0; i < RUN_TIMES; i++) {
            comparisons = 0;
            auto start_time = std::chrono::high_resolution_clock::now();
            heapSort(vector, vector_size, &hs_lastelement_index, &comparisons);
            auto end_time = std::chrono::high_resolution_clock::now();
            dif = std::chrono::duration_cast<std::chrono::milliseconds>(end_time - start_time).count();
            total_dif += dif;
            total_comparisons += comparisons;
            printf("time %d: %ld\n", i + 1, dif);
        }
        total_dif = total_dif / RUN_TIMES;
        printf("total time: %lf\n", total_dif);
        total_comparisons = total_comparisons / RUN_TIMES;
        printf("total comparisons average: %lf\n", total_comparisons);

        result_write(PROGRAM_NAME, vector_size, total_dif, argv[3], total_comparisons);
        if (vector_size <= ARBITRARY_LARGE_SIZE)
            print_vector(vector, vector_size);

        return 0;
    } else {
        // Fails to read full args
        printf("Please fill full args:\n");
        printf("%s.EXE FILENAME FILE_VECTOR_SIZE TEST_TYPE:\n", PROGRAM_NAME);
        printf("Example:\n%s.EXE \"increasing_10.txt\" 10 \"increasing\"\n", PROGRAM_NAME);
        return -1;
    }
}