#!/bin/bash

currentDate=$1

g++ -o quicksort.exe -g src/quicksort/quicksort.cpp -static -O3
for INDEX in 100 500 1000 5000 30000 80000 100000 150000 200000
do
  echo "Quicksort with $INDEX: increasing"
  ./quicksort.exe "increasing_$INDEX.txt" $INDEX "increasing" >> "logs/$currentDate/quicksort_increasing_$INDEX.txt"
  echo "Quicksort with $INDEX: decreasing"
  ./quicksort.exe "decreasing_$INDEX.txt" $INDEX "decreasing" >> "logs/$currentDate/quicksort_decreasing_$INDEX.txt"
  echo "Quicksort with $INDEX: random"
  ./quicksort.exe "random_$INDEX.txt" $INDEX "random" >> "logs/$currentDate/quicksort_random_$INDEX.txt"
done