#!/bin/bash

currentDate=$1

g++ -o number_generator.exe src/number_generator/number_generator.cpp
for INDEX in 100 500 1000 5000 30000 80000 100000 150000 200000
do
  ./number_generator.exe $INDEX >> "logs/$currentDate/number_generator_$INDEX.txt"
done