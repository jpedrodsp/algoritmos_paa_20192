#!/bin/bash

currentDate=$1

g++ -o insertionsort.exe src/insertionsort/insertionsort.cpp -static -O3
for INDEX in 100 500 1000 5000 30000 80000 100000 150000 200000
do
  echo "Insertionsort with $INDEX: increasing"
  ./insertionsort.exe "increasing_$INDEX.txt" $INDEX "increasing" >> "logs/$currentDate/insertionsort_increasing_$INDEX.txt"
  echo "Insertionsort with $INDEX: decreasing"
  ./insertionsort.exe "decreasing_$INDEX.txt" $INDEX "decreasing" >> "logs/$currentDate/insertionsort_decreasing_$INDEX.txt"
  echo "Insertionsort with $INDEX: random"
  ./insertionsort.exe "random_$INDEX.txt" $INDEX "random" >> "logs/$currentDate/insertionsort_random_$INDEX.txt"
done