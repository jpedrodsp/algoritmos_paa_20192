#!/bin/bash

currentDate=$1

g++ -o heapsort.exe -g src/heapsort/heapsort.cpp -static -O3
for INDEX in 100 500 1000 5000 30000 80000 100000 150000 200000
do
  echo "Heapsort with $INDEX: increasing"
  ./heapsort.exe "increasing_$INDEX.txt" $INDEX "increasing" >> "logs/$currentDate/heapsort_increasing_$INDEX.txt"
  echo "Heapsort with $INDEX: decreasing"
  ./heapsort.exe "decreasing_$INDEX.txt" $INDEX "decreasing" >> "logs/$currentDate/heapsort_decreasing_$INDEX.txt"
  echo "Heapsort with $INDEX: random"
  ./heapsort.exe "random_$INDEX.txt" $INDEX "random" >> "logs/$currentDate/heapsort_random_$INDEX.txt"
done