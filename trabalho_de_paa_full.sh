#!/bin/bash

currentDate=`date`

echo "### Creating directories..."
mkdir "logs"
mkdir "logs/$currentDate"

echo "### Generating numbers"
./generate_all_numbers.sh "$currentDate"

echo "### Executing bubblesort"
./bubblesort.sh "$currentDate"

echo "### Executing mergesort"
./mergesort.sh "$currentDate"

echo "### Executing quicksort"
./quicksort.sh "$currentDate"

echo "### Executing insertionsort"
./insertionsort.sh "$currentDate"

echo "### Executing heapsort"
./heapsort.sh "$currentDate"

echo "### Executing hybridsort"
./hybridsort.sh "$currentDate"