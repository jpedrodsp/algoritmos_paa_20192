#!/bin/bash

currentDate=$1

g++ -o mergesort.exe src/mergesort/mergesort.cpp -static -O3
for INDEX in 100 500 1000 5000 30000 80000 100000 150000 200000
do
  echo "Mergesort with $INDEX: increasing"
  ./mergesort.exe "increasing_$INDEX.txt" $INDEX "increasing" >> "logs/$currentDate/mergesort_increasing_$INDEX.txt"
  echo "Mergesort with $INDEX: decreasing"
  ./mergesort.exe "decreasing_$INDEX.txt" $INDEX "decreasing" >> "logs/$currentDate/mergesort_decreasing_$INDEX.txt"
  echo "Mergesort with $INDEX: random"
  ./mergesort.exe "random_$INDEX.txt" $INDEX "random" >> "logs/$currentDate/mergesort_random_$INDEX.txt"
done